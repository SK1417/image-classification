from gradcam import GradCAM
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.applications import imagenet_utils
import warnings
warnings.filterwarnings('ignore')

import numpy as np
import argparse
import imutils
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to the input image")
ap.add_argument("-m", "--model", type=str, required=True, help="model to be used")
args = vars(ap.parse_args())

print("[INFO] loading model...")
model = load_model(args['model'])

orig = cv2.imread(args["image"])
resized = cv2.resize(orig, (224, 224))

image = load_img(args["image"], target_size=(256, 256))
image = img_to_array(image)
image = np.expand_dims(image, axis=0)
image = imagenet_utils.preprocess_input(image)

preds = model.predict(image)

print(preds)

#decoded = imagenet_utils.decode_predictions(preds)
#(imagenetID, label, prob) = decoded[0][0]
prob = preds[0]
print(prob)
label = 'dog' if prob[0]>prob[1] else 'cat'
prob = prob[0] if label =='dog' else prob[1]
label = "{}: {:.2f}%".format(label, prob * 100)
print("[INFO] {}".format(label))

cam = GradCAM(model, prob)
heatmap = cam.compute_heatmap(image)

heatmap = cv2.resize(heatmap, (orig.shape[1], orig.shape[0]))
(heatmap, output) = cam.overlay_heatmap(heatmap, orig, alpha=0.5)

cv2.rectangle(output, (0, 0), (340, 40), (0, 0, 0), -1)
cv2.putText(output, label, (10, 25), cv2.FONT_HERSHEY_SIMPLEX,
	0.8, (255, 255, 255), 2)

output = np.vstack([orig, heatmap, output])
output = imutils.resize(output, height=700)
cv2.imshow("Output", output)
cv2.waitKey(0)
